package com.zhx;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author zhanghaixuan
 * @create 2017/12/17-下午5:31
 **/

@SpringBootApplication
public class SsoServerApplication {

    public static void main(String[] args) {
       SpringApplication.run(SsoServerApplication.class,args);
    }
}
