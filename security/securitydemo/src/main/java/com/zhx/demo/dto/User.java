package com.zhx.demo.dto;

import com.fasterxml.jackson.annotation.JsonView;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Past;
import java.util.Date;

/**
 * 用户实体类
 *
 * @author zhx
 * @create 2017-11-01 10:38
 **/
public class User {
    /*
    Hiberate Validator 用法

    @NotNull             值不能为空
    @Null                值必须为空
    @Pattern(regex=)     字符串必须匹配正则表达式
    @Size(min=,max=)     集合元素的数量必须在min和max之间
    @Email               必须是email地址
    @Length(min=,max=)   字符串的长度必须是多少
    @NotBlank            字符串必须有字符
    @NotEmpty            字符串不为null,集合有元素
    @Range(min=,max=)    数字必须大于 并且小于某个区间
    @SafeHtml            字符串必须是安全得html
    @URl                 字符串必须是合法的url

    @AssertFalse         值必须是false
    @AssertTrue          值必须是true
    @DecimalMax(value=,inclusive=)  inclusive为ture的时候 值必须小于等于value  inclusive为false的时候 值必须大于等于value
    @DecimalMin(value=,inclusive=)  和上面相反    两个都可以注解在字符串上面
    @Digits(integer=,fraction=)     数字格式检查 integer 是整数部分最大长度   fraction 是小数部分最大长度
    @Future   值必须是一个将来的
    @Past     值必须是一个过去的时间
    @Max(value=)
    @Min(value=)
     */



    //JsonView用法  定义一个接口在实体类中
    public interface UserSimpleView{};
    public interface UserDetailView extends UserSimpleView{};


    private String id;

    private String userName;

    //不为空
    @NotBlank(message = "密码不能为空")
    private String passWord;

    @Past(message = "生日必须是过去的时间")
    private Date birthday;

    @JsonView(UserSimpleView.class)
    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    @JsonView(UserSimpleView.class)
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @JsonView(UserSimpleView.class)
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @JsonView(UserDetailView.class)
    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }


    public User(){
    }


    public User(String userName,String passWord){
        this.userName= userName;
        this.passWord = passWord;
    }


    public User(String id,String userName,String passWord,Date birthday){
        this.id = id;
        this.passWord = passWord;
        this.userName = userName;
        this.birthday = birthday;
    }
}
