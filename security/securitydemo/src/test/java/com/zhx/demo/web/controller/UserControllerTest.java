package com.zhx.demo.web.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

/**
 * 用户测试类
 *
 * @author zhx
 * @create 2017-11-01 10:21
 **/
@RunWith(SpringRunner.class)
@SpringBootTest
public class UserControllerTest {

    @Autowired
    private WebApplicationContext wac;

    private MockMvc mockMvc;

    @Before
    public void setup(){
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
    }
    //使用mock作为测试框架
    @Test
    public void query() throws Exception{
        //开始发起请求   MockMvcRequestBuilders 用来发起请求   后面是请求的url

        String content = "{\"userName\":\"tom\",\"page\":1,\"size\":10,}";

        mockMvc.perform(MockMvcRequestBuilders.get("/user").param("userName", "jojo")
                 .param("size", "15")
                 .param("page", "3")
                 .param("sort", "age,desc")
                //请求的时候得传输类型
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                //期望得到的结果  如图是期望得到一个 http 200 的结果
                .andExpect(MockMvcResultMatchers.status().isOk())
                //并且希望得到的结果的长度是3  jsonpath 的具体用法  https://github.com/json-path/JsonPath
                .andExpect(MockMvcResultMatchers.jsonPath("$.code").value("0")
        );
    }

    @Test
    public void queryDetail() throws Exception{
        //开始发起请求   MockMvcRequestBuilders 用来发起请求   后面是请求的url
        mockMvc.perform(MockMvcRequestBuilders.get("/user/1")
                //请求的时候得传输类型
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                //期望得到的结果  如图是期望得到一个 http 200 的结果
                .andExpect(MockMvcResultMatchers.status().isOk())
                //并且希望得到的ID为1  jsonpath 的具体用法  https://github.com/json-path/JsonPath
                .andExpect(MockMvcResultMatchers.jsonPath("$.code").value(0)
                );
    }

    @Test
    public void whenCreateSuccess() throws Exception {

        Date date = new Date();
        System.out.println(date.getTime());
        String content = "{\"userName\":\"tom\",\"passWord\":\"123\",\"birthday\":"+date.getTime()+"}";
        String reuslt = mockMvc.perform(MockMvcRequestBuilders.post("/user").contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(content))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.id").value("1"))
                .andReturn().getResponse().getContentAsString();

        System.out.println(reuslt);
    }

    @Test
    public void whenCreateFail() throws Exception {

        Date date = new Date();
        System.out.println(date.getTime());
        String content = "{\"userName\":\"tom\",\"passWord\":null,\"birthday\":"+date.getTime()+"}";
        String reuslt = mockMvc.perform(MockMvcRequestBuilders.post("/user").contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(content))
//				.andExpect(status().isOk())
//				.andExpect(jsonPath("$.id").value("1"))
                .andReturn().getResponse().getContentAsString();

        System.out.println(reuslt);
    }

    @Test
    public void whenUpdateSuccess() throws Exception {

        Date date = new Date(LocalDateTime.now().minusYears(1).atZone(ZoneId.systemDefault()).toInstant().toEpochMilli());
        System.out.println(date.getTime());
        String content = "{\"id\":\"1\", \"userName\":\"tom\",\"passWord\":\"123\",\"birthday\":"+date.getTime()+"}";
        String reuslt = mockMvc.perform(MockMvcRequestBuilders.put("/user/1").contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(content))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.code").value("0"))
                .andReturn().getResponse().getContentAsString();

        System.out.println(reuslt);
    }

    @Test
    public void whenDeleteSuccess() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/user/1")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void whenUploadSuccess() throws Exception {
        String result = mockMvc.perform(MockMvcRequestBuilders.fileUpload("/file")
                .file(new MockMultipartFile("file", "test.txt", "multipart/form-data", "hello upload".getBytes("UTF-8"))))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn().getResponse().getContentAsString();
        System.out.println(result);
    }

}
