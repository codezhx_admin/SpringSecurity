package com.zhx.result;

/**
 *
 * @author zhx
 * @create 2017-10-23 15:56
 **/

public class Result<T> {

    private static int SUCCESS_CODE = 0;
    private static int ERROR_CODE = 1;


    private static int EXCEPTION_CODE = 500;
    private static int RESOURCE_CODE = 404;
    private static int UNAUTHORIZED_CODE = 401;


    private int code;

    private String msg;

    private T data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public static Result getResult(Object data){
        Result result = new Result();
        result.setMsg("");
        result.setData(data);

        return result;
    }

    public static Result getMsg(String str){
        Result result = new Result();
        result.setMsg(str);
        result.setData("");

        return result;
    }


    public static Result ok(Object data){
        Result result = getResult(data);

        result.setCode(SUCCESS_CODE);

        return result;
    }


    public static Result error(String str){
        Result result = getMsg(str);

        result.setCode(ERROR_CODE);

        return result;
    }

    public static Result exception(String str){
        Result result = getMsg(str);

        result.setCode(EXCEPTION_CODE);

        return result;
    }

    public static Result resource(String str){
        Result result = getMsg(str);

        result.setCode(RESOURCE_CODE);

        return result;
    }

    public static Result unauth(String str){
        Result result = getMsg(str);

        result.setCode(UNAUTHORIZED_CODE);

        return result;
    }
}
