package com.zhx.result;

/**
 * 分页包装实体
 *
 * @author zhx
 * @create 2017-11-01 15:11
 **/
public class Page<T> {

    private int page;

    private int size;

    private Object data;

    public static Page build(int page,int size,Object data){
        return new Page(page,size,data);
    }

    public Page(int page,int size,Object data){
        this.data=data;
        this.page=page;
        this.size=size;
    }


    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
