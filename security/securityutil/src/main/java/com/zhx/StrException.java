package com.zhx;

/**
 * 抛出如警告等异常
 *
 * @author zhx
 * @create 2017-10-24 14:08
 **/
public class StrException extends  RuntimeException {

    public StrException(String message){
        super(message);
    }
}
