package com.zhx.core.validate.code;

import org.springframework.web.context.request.ServletWebRequest;

/**
 * 校验码生产者接口
 * @author zhanghaixuan
 * 2017年11月04日15:58:05
 */
public interface ValidateCodeProcessor {

    /**
     * 校验码放进session之前的前缀
     */
    String SESSION_KEY_PREFIX= "SESSION_KEY_FOR_CODE_";

    /**
     * 创建校验码
     * @param request
     * @throws Exception
     *
     *
     * ServletWebRequest Spring的一个工具类 去封装请求和相应
     */
    void create(ServletWebRequest request) throws Exception;

    /**
     * 校验验证码
     * @param servletWebRequest
     * @throws Exception
     */
    void validate(ServletWebRequest servletWebRequest) throws ValidateCodeException;

}
