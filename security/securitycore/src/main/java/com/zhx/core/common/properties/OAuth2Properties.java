package com.zhx.core.common.properties;

/**
 * @author zhx
 * @create 2017-12-14 15:29
 **/
public class OAuth2Properties {

    private OAuth2ClientProperties[] clients = {};
    /**
     * jwt 验证的秘钥
     */
    private String signKey = "zhx";

    public OAuth2ClientProperties[] getClients() {
        return clients;
    }

    public void setClients(OAuth2ClientProperties[] clients) {
        this.clients = clients;
    }

    public String getSignKey() {
        return signKey;
    }

    public void setSignKey(String signKey) {
        this.signKey = signKey;
    }
}
