package com.zhx.core.social.weixin.connect;

import com.zhx.core.social.weixin.api.WEIXIN;
import com.zhx.core.social.weixin.api.WEIXINImpl;
import org.springframework.social.oauth2.AbstractOAuth2ServiceProvider;

/**
 * @author zhanghaixuan
 * @create 2017/11/19-下午9:29
 **/
public class WEIXINServiceProvider extends AbstractOAuth2ServiceProvider<WEIXIN>{

    /**
     * 微信获取授权码的url
     */
    private static final String URL_AUTHORIZE = "https://open.weixin.qq.com/connect/qrconnect";
    /**
     * 微信获取accessToken的url
     */
    private static final String URL_ACCESS_TOKEN = "https://api.weixin.qq.com/sns/oauth2/access_token";

    /**
     * @param appId
     * @param appSecret
     */
    public WEIXINServiceProvider(String appId, String appSecret) {
        super(new WEIXINAuth2Template(appId, appSecret,URL_AUTHORIZE,URL_ACCESS_TOKEN));
    }


    /* (non-Javadoc)
     * @see org.springframework.social.oauth2.AbstractOAuth2ServiceProvider#getApi(java.lang.String)
     */
    @Override
    public WEIXIN getApi(String accessToken) {
        return new WEIXINImpl(accessToken);
    }
}
