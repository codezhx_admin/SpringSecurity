package com.zhx.core.social.qq.connect;

import com.zhx.core.social.qq.api.QQ;
import com.zhx.core.social.qq.api.QQImpl;
import org.springframework.social.oauth2.AbstractOAuth2ServiceProvider;
import org.springframework.social.oauth2.OAuth2Operations;
import org.springframework.social.oauth2.OAuth2Template;

/**
 * @author zhanghaixuan
 * @create 2017/11/19-下午9:29
 **/
public class QQServiceProvider extends AbstractOAuth2ServiceProvider<QQ>{

    private String appId;

    private static final String URL_GET_AUTHORIZE = "https://graph.qq.com/oauth2.0/authorize";

    private static final String URL_GET_ACCESSTOKEN = "https://graph.qq.com/oauth2.0/token";

    public QQServiceProvider(String appId,String appSecurity) {
        /**
         *
         */

        super(new QQAuth2Template(appId,appSecurity,URL_GET_AUTHORIZE,URL_GET_ACCESSTOKEN));
        this.appId = appId;
    }

    @Override
    public QQ getApi(String accessToken) {
        return new QQImpl(accessToken,appId);
    }
}
