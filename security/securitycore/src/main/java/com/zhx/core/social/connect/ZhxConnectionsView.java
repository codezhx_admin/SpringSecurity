package com.zhx.core.social.connect;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.connect.Connection;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.web.servlet.view.AbstractView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 当绑定用户后的成功页面
 *
 * @author zhanghaixuan
 * @create 2017/11/26-下午8:37
 **/
public class ZhxConnectionsView extends AbstractView {

    @Autowired
    private ObjectMapper objectMapper;

    @Override
    protected void renderMergedOutputModel(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
        //绑定和解除绑定的区别就是这个connection是否为空
        if(model.get("connection") == null){
            response.setContentType("text/html,charset=UTF-8");
            response.getWriter().write("<h2>解绑成功</h2>");
        }else{
            response.setContentType("text/html,charset=UTF-8");
            response.getWriter().write("<h2>绑定成功</h2>");
        }
    }
}
