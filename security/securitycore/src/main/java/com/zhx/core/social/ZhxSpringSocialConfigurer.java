package com.zhx.core.social;

import com.zhx.core.social.SocialAuthenticationFilterPostProcessor;
import com.zhx.core.social.connect.ZhxConnectionsView;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.web.WebMvcProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.social.security.SocialAuthenticationFilter;
import org.springframework.social.security.SpringSocialConfigurer;
import org.springframework.web.servlet.View;


/**
 * 注:此处做了修改
 * @author zhanghaixuan
 * @create 2017/11/25-下午5:27
 **/
public class ZhxSpringSocialConfigurer extends SpringSocialConfigurer {

    private String processesUrl;

    private SocialAuthenticationFilterPostProcessor socialAuthenticationFilterPostProcessor;

    public ZhxSpringSocialConfigurer(String processesUrl){
        this.processesUrl = processesUrl;
    }

    @SuppressWarnings("unChecked")
    @Override
    protected <T> T postProcess(T object) {
        SocialAuthenticationFilter filter = (SocialAuthenticationFilter) super.postProcess(object);
        filter.setFilterProcessesUrl(processesUrl);

        if(socialAuthenticationFilterPostProcessor != null){
            socialAuthenticationFilterPostProcessor.process(filter);
        }

        return super.postProcess(object);
    }

    /**
     * spring social中绑定和解绑成功后默认返回的视图
     * 绑定的请求为 connect/weixin 采用post
     * 解绑的请求为 connect/weixin 采用delete
     * @return
     */
    @Bean({"connect/weixinConnected","connect/weixinConnect"})
    @ConditionalOnMissingBean(name = "weixinConnectView")
    public View weixinConnectView(){
        return new ZhxConnectionsView();
    }

    public String getProcessesUrl() {
        return processesUrl;
    }

    public void setProcessesUrl(String processesUrl) {
        this.processesUrl = processesUrl;
    }

    public SocialAuthenticationFilterPostProcessor getSocialAuthenticationFilterPostProcessor() {
        return socialAuthenticationFilterPostProcessor;
    }

    public void setSocialAuthenticationFilterPostProcessor(SocialAuthenticationFilterPostProcessor socialAuthenticationFilterPostProcessor) {
        this.socialAuthenticationFilterPostProcessor = socialAuthenticationFilterPostProcessor;
    }
}
