package com.zhx.core.authorize;

import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;

/**
 * 授权配置的接口 不同的业务和模块都通过实现这个接口向系统添加不需要登录就能授权通过的地址
 *
 * @author zhx
 * @create 2017-12-18 9:17
 **/
public interface AuthorizeConfigProvider {

    /**
     * 返回的boolena 表示配置中是否有针对anyRequest的配置,在整个授权配置中,应该有且仅有一个针对anyRequest
     * 的配置,如果所有的实现都没有针对anyRequest的配置,系统会自动增加一个anyRequest().authenticated()的配置.
     * 如果有多个针对anyRequest的配置 就会抛出异常
     * @param config
     * @return
     */
    boolean config(ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry config);
}
