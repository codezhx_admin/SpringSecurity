package com.zhx.core.social;

import com.zhx.core.common.properties.SecurityProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.encrypt.Encryptors;
import org.springframework.social.config.annotation.EnableSocial;
import org.springframework.social.config.annotation.SocialConfigurerAdapter;
import org.springframework.social.connect.ConnectionFactoryLocator;
import org.springframework.social.connect.ConnectionSignUp;
import org.springframework.social.connect.UsersConnectionRepository;
import org.springframework.social.connect.jdbc.JdbcUsersConnectionRepository;
import org.springframework.social.connect.web.ProviderSignInUtils;
import org.springframework.social.security.SpringSocialConfigurer;

import javax.sql.DataSource;

/**
 * 社交配置的一个适配器
 * @author zhanghaixuan
 * @create 2017/11/25-上午10:29
 **/
@Configuration
@EnableSocial
public class ScoialConfig extends SocialConfigurerAdapter{

    @Qualifier("dataSource")
    @Autowired
    private DataSource dataSource;

    @Autowired
    private SecurityProperties securityProperties;

    /**
     * 注:加入required = false 的情况下
     */
    @Autowired(required = false)
    private ConnectionSignUp signUp;

    @Autowired(required = false)
    private SocialAuthenticationFilterPostProcessor socialAuthenticationFilterPostProcessor;

//    @Autowired(required = false)
//    private ConnectionSignUp connectionSignUp;


    /**
     * springboot 配置好的  也可以直接写在方法的参数上 也可以直接注入
     */
//    @Autowired
//    private ConnectionFactoryLocator connectionFactoryLocator;

    /**
     *
     * @param connectionFactoryLocator 去查找系统中的 connectionFactory
     * @return
     */
    @Override
    public UsersConnectionRepository getUsersConnectionRepository(ConnectionFactoryLocator connectionFactoryLocator) {
        /**
         * 三个参数构造连接方法
         * 第一个参数是数据源
         * 第二个参数是去匹配当前你需要的connectionFactory 微博 WEIXIN 微信都会有自己的connectionFactory
         * 第三个参数是一个加解密的操作 因为获取的信息都是敏感的
         *
         * 这里弄完之后需要去创建保存的数据库  点击 JdbcUsersConnectionRepository 找到文件 文件下方有SQL语句
         */

        JdbcUsersConnectionRepository jdbcUsersConnectionRepository = new JdbcUsersConnectionRepository(dataSource, connectionFactoryLocator, Encryptors.noOpText());
        //下方是演示假设你的创建的数据表需要具备前缀的操作方法 注意表名不能修改
        //jdbcUsersConnectionRepository.setTablePrefix("zhx_");
        //如果实现了signup的情况下才去注册
//        if(connectionSignUp != null) {
//            jdbcUsersConnectionRepository.setConnectionSignUp(signUp);
//        }

        return jdbcUsersConnectionRepository;
    }


    @Bean
    public SpringSocialConfigurer zhxSocialSpringConfigurer(){
        String filterProcessesUrl = securityProperties.getSocial().getProcessesUrl();
        ZhxSpringSocialConfigurer configurer = new ZhxSpringSocialConfigurer(filterProcessesUrl);


        //浏览器中跳转
        configurer.signupUrl(securityProperties.getBrowser().getSignUpUrl());
        configurer.setSocialAuthenticationFilterPostProcessor(socialAuthenticationFilterPostProcessor);
        return configurer;
    }

    /**
     * ProviderSignInUtils spring social提供的工具类
     * 解决两个问题:
     * 1.在用户注册的过程用如何拿到spring social的信息
     * 2.第二个是如何在用户注册完成后把系统生成的用户ID返回给spring social
     */
    @Bean
    public ProviderSignInUtils providerSignInUtils(ConnectionFactoryLocator connectionFactoryLocator){
        return new ProviderSignInUtils(connectionFactoryLocator,getUsersConnectionRepository(connectionFactoryLocator));
    }

}
