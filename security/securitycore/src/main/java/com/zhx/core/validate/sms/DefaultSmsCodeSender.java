package com.zhx.core.validate.sms;

import org.springframework.stereotype.Component;

/**
 * 默认的短信发送方法
 * Created by zhanghaixuan on 2017/11/4.
 */
@Component("smsCodeSender")
public class DefaultSmsCodeSender implements SmsCodeSender {

    @Override
    public void send(String moblie, String code) {
        System.out.println("发送手机:"+moblie+",短信验证码为:"+code);
    }
}
