package com.zhx.core.common.properties;

import com.zhx.core.common.constants.SecurityConstants;

/**
 * @author zhanghaixuan
 * @create 2017/11/26-下午10:02
 **/
public class SessionProperties {
    private int maximumSessions = 1;

    private boolean maxSessionsPreventsLogin = false;

    private String invalidSessionUrl = SecurityConstants.SESSION_VALIED_URL;

    public int getMaximumSessions() {
        return maximumSessions;
    }

    public void setMaximumSessions(int maximumSessions) {
        this.maximumSessions = maximumSessions;
    }

    public boolean isMaxSessionsPreventsLogin() {
        return maxSessionsPreventsLogin;
    }

    public void setMaxSessionsPreventsLogin(boolean maxSessionsPreventsLogin) {
        this.maxSessionsPreventsLogin = maxSessionsPreventsLogin;
    }

    public String getInvalidSessionUrl() {
        return invalidSessionUrl;
    }

    public void setInvalidSessionUrl(String invalidSessionUrl) {
        this.invalidSessionUrl = invalidSessionUrl;
    }
}
