package com.zhx.core.validate.code;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 图形验证码实体
 * Created by zhanghaixuan on 2017/11/2.
 */
public class ValidateCode implements Serializable {

    private String code;

    private LocalDateTime localDateTime;

    public boolean isExpried() {
        return LocalDateTime.now().isAfter(localDateTime);
    }

    public ValidateCode(String code, int expireIn){
        this.code = code;
        this.localDateTime = LocalDateTime.now().plusSeconds(expireIn);
    }

    public ValidateCode(String code, LocalDateTime localDateTime){
        this.code = code;
        this.localDateTime = localDateTime;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public LocalDateTime getLocalDateTime() {
        return localDateTime;
    }

    public void setLocalDateTime(LocalDateTime localDateTime) {
        this.localDateTime = localDateTime;
    }
}
