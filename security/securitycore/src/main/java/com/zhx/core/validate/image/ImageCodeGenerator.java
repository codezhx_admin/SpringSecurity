package com.zhx.core.validate.image;

import com.zhx.core.common.properties.SecurityProperties;
import com.zhx.core.validate.code.ValidateCodeGenerator;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.request.ServletWebRequest;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Random;

/**
 * 验证码的生成器
 *
 * @author zhx
 * @create 2017-11-03 11:44
 **/
public class ImageCodeGenerator implements ValidateCodeGenerator {

    private SecurityProperties securityProperties;

    @Override
    public ImageCode generate(ServletWebRequest request){

        int width= ServletRequestUtils.getIntParameter(request.getRequest(),"width", securityProperties.getValideDateCode().getImageCode().getWidth());

        int height= ServletRequestUtils.getIntParameter(request.getRequest(),"height",securityProperties.getValideDateCode().getImageCode().getHeight());

        BufferedImage bufferedImage = new BufferedImage(width,height,BufferedImage.TYPE_INT_RGB);
        Random random = new Random();
        Graphics g = bufferedImage.getGraphics();
        g.setColor(getRandColor(200,250));
        g.fillRect(0,0,width,height);
        g.setFont(new Font("Time New Roman",Font.ITALIC,20));
        g.setColor(getRandColor(160,200));

        for(int i=0;i<155;i++){
            int x = random.nextInt(width);
            int y = random.nextInt(height);
            int xl = random.nextInt(12);
            int yl = random.nextInt(12);
            g.drawLine(x,y,x+xl,y+yl);
        }

        String sRand="";

        int length = securityProperties.getValideDateCode().getImageCode().getLength();

        for(int i=0;i<length;i++){
            String rand = String.valueOf(random.nextInt(10));
            sRand+=rand;
            g.setColor(new Color(20+random.nextInt(110),20+random.nextInt(110),20+random.nextInt(110)));
            g.drawString(rand,13*i+6,16);
        }
        g.dispose();
        return new ImageCode(bufferedImage,sRand,securityProperties.getValideDateCode().getImageCode().getExpireIn());
    }

    private Color getRandColor(int fc, int bc){
        Random random = new Random();
        if(fc>255){
            fc=255;
        }
        if(bc>255){
            bc=255;
        }
        int r = fc+random.nextInt(bc-fc);
        int g = fc+random.nextInt(bc-fc);
        int b = fc+random.nextInt(bc-fc);
        return new Color(r,g,b);
    }


    public void setSecurityProperties(SecurityProperties securityProperties) {
        this.securityProperties = securityProperties;
    }
    public SecurityProperties getSecurityProperties() {
        return securityProperties;
    }
}
