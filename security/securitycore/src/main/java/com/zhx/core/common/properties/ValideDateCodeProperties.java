package com.zhx.core.common.properties;

/**
 * 短信等验证码封装
 *
 * @author zhx
 * @create 2017-11-03 10:07
 **/
public class ValideDateCodeProperties {

    private  ImageCodeProperties imageCode = new ImageCodeProperties();

    private  SmsCodeProperties smsCode = new SmsCodeProperties();

    public SmsCodeProperties getSmsCode() {
        return smsCode;
    }

    public void setSmsCode(SmsCodeProperties smsCode) {
        this.smsCode = smsCode;
    }

    public ImageCodeProperties getImageCode() {
        return imageCode;
    }

    public void setImageCode(ImageCodeProperties imageCode) {
        this.imageCode = imageCode;
    }
}
