package com.zhx.core.validate.code;

import com.zhx.core.common.properties.SecurityProperties;
import com.zhx.core.validate.image.ImageCodeGenerator;
import com.zhx.core.validate.sms.DefaultSmsCodeSender;
import com.zhx.core.validate.sms.SmsCodeSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 可配置的图形验证码配置
 *
 * 如下写法是和spring在 class 上面注明 @Compent("imageCodeGenerator") 效果是一样的  现在为手工在配置的时候注入一个bean
 *
 * @ConditionalOnMissingBean
 * 当在Bean 中注入这个注解的时候  会首先先spring的容器中去找一个和name相同的bean
 *
 * 假设他找到了这个名字的bean 就不会使用下面代码返回的bean 而使用代码中的bean
 *
 * @author zhx
 * @create 2017-11-03 13:46
 **/
@Configuration
public class ValidateCodeBeanConfig {

    @Autowired
    private SecurityProperties securityProperties;

    @Bean
    @ConditionalOnMissingBean(name = "imageCodeGenerator" )
    public  ValidateCodeGenerator imageCodeGenerator(){
        ImageCodeGenerator codeGenerator = new ImageCodeGenerator();
        codeGenerator.setSecurityProperties(securityProperties);
        return codeGenerator;
    }

    /**
     * @ConditionalOnMissingBean(SmsCodeSender.class)
     *
     * 注解的另外一种用法 假设已经发现了这个接口的实现 就不会去用下面的实现了
     */
    @Bean
    @ConditionalOnMissingBean(name = "smsCodeGenerator")
    public SmsCodeSender smsCodeGenerator(){
        DefaultSmsCodeSender defaultSmsCodeSender = new DefaultSmsCodeSender();
        return defaultSmsCodeSender;
    }


    @Bean
    @ConditionalOnMissingBean(name = "validateCodeFilter")
    public ValidateCodeFilter validateCodeFilter(){
        ValidateCodeFilter validateCodeFilter = new ValidateCodeFilter();
        return validateCodeFilter;
    }


}
