package com.zhx.core.social.qq.api;

import java.io.IOException;

/**
 * 此接口用来获取QQ用户信息
 * @author zhanghaixuan
 * @create 2017/11/19-下午8:35
 **/
public interface QQ {
    QQUserInfo getQQUserInfo();
}
