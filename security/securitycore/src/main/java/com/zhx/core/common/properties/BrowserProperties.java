package com.zhx.core.common.properties;

import com.zhx.core.common.LoginType;
import com.zhx.core.common.constants.SecurityConstants;

/**
 * 浏览器端Securiy 配置项目
 *
 * @author zhx
 * @create 2017-11-02 11:08
 **/
public class BrowserProperties {
    private String loginPage = SecurityConstants.DEFAULT_LOGIN_P;

    private LoginType loginType = LoginType.JSON;


    private int rememberMeSeconds = 3600;

    private String signUpUrl = "/zhx-signUp.html";

    private String signInUrl = "/zhx-signIn.html";

    private String signOutUrl = "";

    private String logout = "";


    public String getSignOutUrl() {
        return signOutUrl;
    }

    public void setSignOutUrl(String signOutUrl) {
        this.signOutUrl = signOutUrl;
    }

    public String getSignInUrl() {
        return signInUrl;
    }

    public void setSignInUrl(String signInUrl) {
        this.signInUrl = signInUrl;
    }

    public String getLogout() {
        return logout;
    }

    public void setLogout(String logout) {
        this.logout = logout;
    }

    private SessionProperties session = new SessionProperties();

    public SessionProperties getSession() {
        return session;
    }

    public void setSession(SessionProperties session) {
        this.session = session;
    }

    public String getSignUpUrl() {
        return signUpUrl;
    }

    public void setSignUpUrl(String signUpUrl) {
        this.signUpUrl = signUpUrl;
    }

    public int getRememberMeSeconds() {
        return rememberMeSeconds;
    }

    public void setRememberMeSeconds(int rememberMeSeconds) {
        this.rememberMeSeconds = rememberMeSeconds;
    }

    public LoginType getLoginType() {
        return loginType;
    }

    public void setLoginType(LoginType loginType) {
        this.loginType = loginType;
    }

    public String getLoginPage() {
        return loginPage;
    }

    public void setLoginPage(String loginPage) {
        this.loginPage = loginPage;
    }
}
