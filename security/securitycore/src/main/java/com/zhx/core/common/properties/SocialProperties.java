package com.zhx.core.common.properties;

/**
 * @author zhanghaixuan
 * @create 2017/11/25-上午11:08
 **/
public class SocialProperties {

    private QQproperties qq = new QQproperties();

    private WeixinProperties weixin = new WeixinProperties();
    
    private String processesUrl = "";

    public String getProcessesUrl() {
        return processesUrl;
    }

    public void setProcessesUrl(String processesUrl) {
        this.processesUrl = processesUrl;
    }

    public WeixinProperties getWeixin() {
        return weixin;
    }

    public void setWeixin(WeixinProperties weixin) {
        this.weixin = weixin;
    }

    public QQproperties getQq() {
        return qq;
    }

    public void setQq(QQproperties qq) {
        this.qq = qq;
    }
}
