package com.zhx.core.common.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 主配置项目  所有配置封装在此处
 * @author zhx
 * @create 2017-11-02 11:08
 **/
@Component
@ConfigurationProperties(prefix = "zhx.security")
public class SecurityProperties {

    private BrowserProperties browser = new BrowserProperties();

    private ValideDateCodeProperties valideDateCode = new ValideDateCodeProperties();

    private SocialProperties social = new SocialProperties();

    private OAuth2Properties oAuth2Properties = new OAuth2Properties();

    public OAuth2Properties getoAuth2Properties() {
        return oAuth2Properties;
    }

    public void setoAuth2Properties(OAuth2Properties oAuth2Properties) {
        this.oAuth2Properties = oAuth2Properties;
    }

    public SocialProperties getSocial() {
        return social;
    }

    public void setSocial(SocialProperties social) {
        this.social = social;
    }

    public ValideDateCodeProperties getValideDateCode() {
        return valideDateCode;
    }

    public void setValideDateCode(ValideDateCodeProperties valideDateCode) {
        this.valideDateCode = valideDateCode;
    }

    public BrowserProperties getBrowser() {
        return browser;
    }

    public void setBrowser(BrowserProperties browser) {
        this.browser = browser;
    }
}
