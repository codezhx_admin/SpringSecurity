package com.zhx.core.common;

/**
 * 自定义的登陆类型枚举
 *
 * @author zhx
 * @create 2017-11-02 11:43
 **/
public enum LoginType {

    REDIECT,
    JSON
}
