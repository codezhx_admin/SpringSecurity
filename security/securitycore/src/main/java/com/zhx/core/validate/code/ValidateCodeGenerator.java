package com.zhx.core.validate.code;

import org.springframework.web.context.request.ServletWebRequest;

/**
 * 生成验证码的接口
 *
 * @author zhx
 * @create 2017-11-03 11:46
 **/
public interface ValidateCodeGenerator {

    /**
     * V1生成图形验证码接口
     *
     * V2 重构代码  使用继承关系  父类为验证码,图形验证码是验证码的一个子类
     * @param request
     * @return
     */
    ValidateCode generate(ServletWebRequest request);
}
