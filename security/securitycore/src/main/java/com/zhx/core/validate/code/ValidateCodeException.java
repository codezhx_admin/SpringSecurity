package com.zhx.core.validate.code;


import org.springframework.security.core.AuthenticationException;

/**
 * Created by zhanghaixuan on 2017/11/2.
 */
public class ValidateCodeException extends AuthenticationException {

    /**
     *
     */
    private static final long serialVersionUID = -15615321321032032L;

    public  ValidateCodeException(String msg){
        super(msg);
    }

}
