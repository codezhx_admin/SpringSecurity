package com.zhx.core.social.connect;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.org.apache.xpath.internal.operations.Bool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.ConnectionUtils;
import org.springframework.social.connect.Connection;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.web.servlet.view.AbstractView;
import org.w3c.dom.views.DocumentView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author zhanghaixuan
 * @create 2017/11/26-下午8:37
 **/
@Component("connect/status")
public class ZhxConnectionsStatusView extends AbstractView {

    @Autowired
    private ObjectMapper objectMapper;

    @Override
    protected void renderMergedOutputModel(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {

        Map<String, List<Connection<?>>> connections = (Map<String, List<Connection<?>>>) model.get("connectionMap");


        Map<String,Boolean> returnMap = new HashMap<>();

        for(String key:connections.keySet()){
            returnMap.put(key, !CollectionUtils.isEmpty(connections.get(key)));

        }

        response.setContentType("application/json,charset=UTF-8");
        response.getWriter().write(objectMapper.writeValueAsString(returnMap));
//        Map<String, List<Connection<?>>> connections = connectionRepository.findAllConnections();
//        model.addAttribute("providerIds", connectionFactoryLocator.registeredProviderIds());
//        model.addAttribute("connectionMap", connections);

    }
}
