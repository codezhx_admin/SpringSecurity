package com.zhx.core.authorize;

import com.zhx.core.common.constants.SecurityConstants;
import com.zhx.core.common.properties.SecurityProperties;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.stereotype.Component;

/**
 * 核心模块的授权配置提供器,安全模块涉及的url的授权配置都在这里
 *
 * @author zhx
 * @create 2017-12-18 9:27
 **/
@Component
@Order(Integer.MIN_VALUE)
public class ZhxAuthorizeConfigProvider implements AuthorizeConfigProvider {
    @Autowired
    private SecurityProperties securityProperties;

    /*
     *  抽象了系统中的所有验证链接
     */
    @Override
    public boolean config(ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry config) {

        config.antMatchers(SecurityConstants.DEFAULT_LOGIN_PROCESSING_URL_FORM,
                SecurityConstants.DEFAULT_LOGIN_PROCESSING_URL_MOBILE,
                SecurityConstants.DEFAULT_LOGIN_PROCESSING_URL_OPENID,
                SecurityConstants.DEFAULT_VAILDATE_CODE_URL_PREFIX+"/*",
                securityProperties.getBrowser().getSignUpUrl(),
                securityProperties.getBrowser().getSignInUrl(),
                securityProperties.getBrowser().getSession().getInvalidSessionUrl()).permitAll();

        if(StringUtils.isNotBlank(securityProperties.getBrowser().getSignOutUrl())){
            config.antMatchers(securityProperties.getBrowser().getSignOutUrl()).permitAll();
        }

        return false;
    }
}
