package com.zhx.core.social.weixin.api;

/**
 * 此接口用来获取QQ用户信息
 * @author zhanghaixuan
 * @create 2017/11/19-下午8:35
 **/
public interface WEIXIN {
    /**
     * 注意微信的这个借口需要传入openid
     * @param openId
     * @return
     */
    WEIXINUserInfo getUserInfo(String openId);
}
