package com.zhx.core.validate.code;

import com.zhx.core.common.ValidateCodeType;
import org.springframework.web.context.request.ServletWebRequest;

/**
 * 验证码生成获取移除逻辑封装
 *
 * @author zhx
 * @create 2017-12-13 15:13
 **/
public interface ValidateCodeRepository {
    /**
     * 保存验证码
     * @param request
     * @param code
     * @param validateCodeType
     */
    void save(ServletWebRequest request,ValidateCode code,ValidateCodeType validateCodeType);

    /**
     * 获取验证码
     * @param request
     * @param validateCodeType
     */
    ValidateCode  get(ServletWebRequest request,ValidateCodeType validateCodeType);

    /**
     * 移除验证码
     * @param request
     * @param validateCodeType
     */
    void remove(ServletWebRequest request,ValidateCodeType validateCodeType);
}
