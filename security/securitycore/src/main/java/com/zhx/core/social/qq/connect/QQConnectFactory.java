package com.zhx.core.social.qq.connect;

import com.zhx.core.social.qq.api.QQ;
import org.springframework.social.connect.ApiAdapter;
import org.springframework.social.connect.support.OAuth2ConnectionFactory;
import org.springframework.social.oauth2.OAuth2ServiceProvider;

/**
 * @author zhanghaixuan
 * @create 2017/11/25-上午10:24
 **/
public class QQConnectFactory extends OAuth2ConnectionFactory<QQ> {

    public QQConnectFactory(String providerId, String appId,String appSerurit) {
        super(providerId, new QQServiceProvider(appId,appSerurit), new QQAdapter());
    }
}
