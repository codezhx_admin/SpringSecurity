package com.zhx.core.common.properties;

/**
 * 短信验证码可配置选项
 * @author zhx
 * @create 2017-11-03 10:05
 **/
public class SmsCodeProperties {
    private int length = 6;

    private int expireIn = 300;

    /**
     * 配置的需要拦截验证码的链接 用逗号隔开
     */
    private  String urls;

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getExpireIn() {
        return expireIn;
    }

    public void setExpireIn(int expireIn) {
        this.expireIn = expireIn;
    }

    public String getUrls() {
        return urls;
    }

    public void setUrls(String urls) {
        this.urls = urls;
    }
}
