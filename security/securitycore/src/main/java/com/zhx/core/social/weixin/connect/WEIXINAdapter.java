package com.zhx.core.social.weixin.connect;

import com.zhx.core.social.weixin.api.WEIXIN;
import com.zhx.core.social.weixin.api.WEIXINUserInfo;
import org.springframework.social.connect.ApiAdapter;
import org.springframework.social.connect.ConnectionValues;
import org.springframework.social.connect.UserProfile;

/**
 * @author zhanghaixuan
 * @create 2017/11/25-上午10:08
 **/
public class WEIXINAdapter implements ApiAdapter<WEIXIN> {

    private String openId;

    public WEIXINAdapter() {}

    public WEIXINAdapter(String openId){
        this.openId = openId;
    }

    /**
     * @param api
     * @return
     */
    @Override
    public boolean test(WEIXIN api) {
        return true;
    }

    /**
     * @param api
     * @param values
     */
    @Override
    public void setConnectionValues(WEIXIN api, ConnectionValues values) {
        WEIXINUserInfo profile = api.getUserInfo(openId);
        values.setProviderUserId(profile.getOpenid());
        values.setDisplayName(profile.getNickname());
        values.setImageUrl(profile.getHeadimgurl());
    }

    /**
     * @param api
     * @return
     */
    @Override
    public UserProfile fetchUserProfile(WEIXIN api) {
        return null;
    }

    /**
     * @param api
     * @param message
     */
    @Override
    public void updateStatus(WEIXIN api, String message) {
        //do nothing
    }
}
