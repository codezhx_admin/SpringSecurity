package com.zhx.core.social.weixin.connect;

import com.zhx.core.social.weixin.api.WEIXIN;
import org.springframework.social.connect.ApiAdapter;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionData;
import org.springframework.social.connect.support.OAuth2Connection;
import org.springframework.social.connect.support.OAuth2ConnectionFactory;
import org.springframework.social.oauth2.AccessGrant;
import org.springframework.social.oauth2.OAuth2ServiceProvider;

/**
 * @author zhanghaixuan
 * @create 2017/11/25-上午10:24
 **/
public class WEIXINConnectFactory extends OAuth2ConnectionFactory<WEIXIN> {
    /**
     * @param appId
     * @param appSecret
     */
    public WEIXINConnectFactory(String providerId, String appId, String appSecret) {
        super(providerId, new WEIXINServiceProvider(appId, appSecret), new WEIXINAdapter());
    }

    /**
     * 由于微信的openId是和accessToken一起返回的，所以在这里直接根据accessToken设置providerUserId即可，不用像QQ那样通过QQAdapter来获取
     */
    @Override
    protected String extractProviderUserId(AccessGrant accessGrant) {
        if(accessGrant instanceof WeixinAccessGrant) {
            return ((WeixinAccessGrant)accessGrant).getOpenId();
        }
        return null;
    }

    /* (non-Javadoc)
     * @see org.springframework.social.connect.support.OAuth2ConnectionFactory#createConnection(org.springframework.social.oauth2.AccessGrant)
     */
    @Override
    public Connection<WEIXIN> createConnection(AccessGrant accessGrant) {
        return new OAuth2Connection<WEIXIN>(getProviderId(), extractProviderUserId(accessGrant), accessGrant.getAccessToken(),
                accessGrant.getRefreshToken(), accessGrant.getExpireTime(), getOAuth2ServiceProvider(), getApiAdapter(extractProviderUserId(accessGrant)));
    }

    /* (non-Javadoc)
     * @see org.springframework.social.connect.support.OAuth2ConnectionFactory#createConnection(org.springframework.social.connect.ConnectionData)
     */
    @Override
    public Connection<WEIXIN> createConnection(ConnectionData data) {
        return new OAuth2Connection<WEIXIN>(data, getOAuth2ServiceProvider(), getApiAdapter(data.getProviderUserId()));
    }

    private ApiAdapter<WEIXIN> getApiAdapter(String providerUserId) {
        return new WEIXINAdapter(providerUserId);
    }

    private OAuth2ServiceProvider<WEIXIN> getOAuth2ServiceProvider() {
        return (OAuth2ServiceProvider<WEIXIN>) getServiceProvider();
    }
}
