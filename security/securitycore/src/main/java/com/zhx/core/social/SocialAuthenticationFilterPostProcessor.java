package com.zhx.core.social;

import org.springframework.social.security.SocialAuthenticationFilter;

/**
 * 抽取社交用户获取完openid后
 * @author zhx
 * @create 2017-12-14 10:55
 **/



public interface SocialAuthenticationFilterPostProcessor {
    void process(SocialAuthenticationFilter socialAuthenticationFilter);
}
