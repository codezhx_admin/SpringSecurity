package com.zhx.core.common.properties;

import org.springframework.boot.autoconfigure.social.SocialProperties;

/**
 * @author zhanghaixuan
 * @create 2017/11/25-上午11:05
 **/
public class QQproperties extends SocialProperties{
    /**
     * 服务提供商的标识
     */
    private String providerId="qq";

    public String getProviderId() {
        return providerId;
    }

    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }
}
