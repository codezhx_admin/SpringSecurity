package com.zhx.core.social.qq.connect;

import com.zhx.core.social.qq.api.QQ;
import com.zhx.core.social.qq.api.QQUserInfo;
import org.springframework.social.connect.ApiAdapter;
import org.springframework.social.connect.ConnectionValues;
import org.springframework.social.connect.UserProfile;

import java.io.IOException;

/**
 * @author zhanghaixuan
 * @create 2017/11/25-上午10:08
 **/
public class QQAdapter implements ApiAdapter<QQ> {
    @Override
    public boolean test(QQ qq) {
        return true;
    }

    /**
     * 设置好 connectionValues 里面的东西
     * @param qq
     * @param connectionValues
     */
    @Override
    public void setConnectionValues(QQ qq, ConnectionValues connectionValues) {
            QQUserInfo qqUserInfo = qq.getQQUserInfo();
            //用户名称
            connectionValues.setDisplayName(qqUserInfo.getNickname());
            //头像
            connectionValues.setImageUrl(qqUserInfo.getFigureurl_qq_1());
            //个人主页
            connectionValues.setProfileUrl("");
            //服务商的用户ID 就是我们用的
            connectionValues.setProviderUserId(qqUserInfo.getOpenId());
    }

    @Override
    public UserProfile fetchUserProfile(QQ qq) {
        return null;
    }

    /**
     * 更新原应用的状态 QQ不需要
     * @param qq
     * @param s
     */
    @Override
    public void updateStatus(QQ qq, String s) {

    }
}
