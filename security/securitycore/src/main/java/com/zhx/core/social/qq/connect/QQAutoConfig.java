package com.zhx.core.social.qq.connect;

import com.zhx.core.common.properties.SecurityProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.social.SocialAutoConfigurerAdapter;
import org.springframework.context.annotation.Configuration;
import org.springframework.social.connect.ConnectionFactory;

/**
 * ConditionalOnProperty 这个注解指的是假设我的配置文件中没有设置这个项目 则这个配置文件不起作用
 *
 * @author zhanghaixuan
 * @create 2017/11/25-上午11:10
 **/
@Configuration
@ConditionalOnProperty(prefix = "zhx.security.social.qq",name = "app-id")
public class QQAutoConfig extends SocialAutoConfigurerAdapter{
    @Autowired
    private SecurityProperties securityProperties;

    @Override
    protected ConnectionFactory<?> createConnectionFactory() {
        return new QQConnectFactory(securityProperties.getSocial().getQq().getProviderId(),securityProperties.getSocial().getQq().getAppId(),securityProperties.getSocial().getQq().getAppSecret());
    }
}
