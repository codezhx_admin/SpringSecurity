package com.zhx.core.common.properties;

/**
 * 验证码可配置选项
 * @author zhx
 * @create 2017-11-03 10:05
 **/
public class ImageCodeProperties extends SmsCodeProperties{

    public ImageCodeProperties(){
        setLength(4);
        setExpireIn(60);
    }

    private int width = 67;

    private int height = 23;


    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }
}
