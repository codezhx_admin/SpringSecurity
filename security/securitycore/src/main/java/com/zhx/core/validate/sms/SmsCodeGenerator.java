package com.zhx.core.validate.sms;

import com.zhx.core.common.properties.SecurityProperties;
import com.zhx.core.validate.code.ValidateCode;
import com.zhx.core.validate.code.ValidateCodeGenerator;
import org.apache.commons.lang.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.ServletWebRequest;

/**
 * 验证码的生成器
 *
 * @author zhx
 * @create 2017-11-03 11:44
 **/
@Component("smsCodeGenerator")
public class SmsCodeGenerator implements ValidateCodeGenerator {

    @Autowired
    private SecurityProperties securityProperties;

    @Override
    public ValidateCode generate(ServletWebRequest request){

        String code = RandomStringUtils.randomNumeric(securityProperties.getValideDateCode().getSmsCode().getLength());

        return new ValidateCode(code,securityProperties.getValideDateCode().getSmsCode().getExpireIn());
    }
}
