/**
 * 
 */
package com.zhx.security.rbac.repository;

import org.springframework.stereotype.Repository;

import com.zhx.security.rbac.domain.Role;

/**
 * @author zhailiang
 *
 */
@Repository
public interface RoleRepository extends ImoocRepository<Role> {

}
