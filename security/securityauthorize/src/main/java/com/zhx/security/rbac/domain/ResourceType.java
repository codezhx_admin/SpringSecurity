/**
 * 
 */
package com.zhx.security.rbac.domain;

/**
 * @author zhailiang
 *
 */
public enum ResourceType {
	
	MENU,
	
	BUTTON

}
