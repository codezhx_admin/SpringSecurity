/**
 * 
 */
package com.zhx.security.rbac.repository;

import org.springframework.stereotype.Repository;

import com.zhx.security.rbac.domain.RoleAdmin;

/**
 * @author zhailiang
 *
 */
@Repository
public interface RoleAdminRepository extends ImoocRepository<RoleAdmin> {

}
