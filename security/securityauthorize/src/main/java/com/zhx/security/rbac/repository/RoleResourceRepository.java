/**
 * 
 */
package com.zhx.security.rbac.repository;

import org.springframework.stereotype.Repository;

import com.zhx.security.rbac.domain.RoleResource;

/**
 * @author zhailiang
 *
 */
@Repository
public interface RoleResourceRepository extends ImoocRepository<RoleResource> {

}
