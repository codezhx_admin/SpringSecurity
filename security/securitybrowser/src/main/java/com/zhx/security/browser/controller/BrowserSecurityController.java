package com.zhx.security.browser.controller;

import com.zhx.core.common.properties.SecurityProperties;
import com.zhx.core.social.support.SocialUserInfo;
import com.zhx.result.Result;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.web.ProviderSignInUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.ServletWebRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 处理是否需要身份验证的controller
 *
 * @author zhx
 * @create 2017-11-02 10:50
 **/
@RestController
public class BrowserSecurityController {
    private  final static Logger logger = LoggerFactory.getLogger(BrowserSecurityController.class);

    //HttpSessionRequestCache spring 获取之前缓存的请求
    private RequestCache requestCache = new HttpSessionRequestCache();
    //用来做跳转的类
    private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

    @Autowired
    private SecurityProperties securityProperties;

    @Autowired
    private ProviderSignInUtils providerSignInUtils;

    @RequestMapping("authentication/require")
    @ResponseStatus(code = HttpStatus.UNAUTHORIZED)
    public Result requireAuthentication(HttpServletRequest request, HttpServletResponse response) throws IOException {

        SavedRequest saveRequest = requestCache.getRequest(request, response);

        if(saveRequest != null){
            String targetUrl = saveRequest.getRedirectUrl();

            logger.info("BrowserSecurityController 中 引发跳转的请求是:{}" ,targetUrl);
            //引发请求的是html
            if(StringUtils.endsWithIgnoreCase(targetUrl,".html")){
                redirectStrategy.sendRedirect(request,response,securityProperties.getBrowser().getLoginPage() );
            }
        }
        return Result.unauth("访问的请求需要身份认证,请引导用户到登录页");
    }

    /**
     * 从spring Social中获取当前用户的第三方信息
     * @param request
     * @return
     */
    @GetMapping("/social/user")
    private Result<SocialUserInfo> getSocialUserInfo(HttpServletRequest request){
        SocialUserInfo socialUserInfo = new SocialUserInfo();

        Connection<?> connectionFromSession = providerSignInUtils.getConnectionFromSession(new ServletWebRequest(request));

        socialUserInfo.setProviderId(connectionFromSession.getKey().getProviderId());
        socialUserInfo.setProviderUserId(connectionFromSession.getKey().getProviderUserId());
        socialUserInfo.setHeadimg(connectionFromSession.getImageUrl());
        socialUserInfo.setNickname(connectionFromSession.getDisplayName());

        return Result.ok(socialUserInfo);
    }


    @GetMapping("/session/invalid")
    @ResponseStatus(code=HttpStatus.UNAUTHORIZED)
    public Result sessionInvalid(){
        String message="session过期";
        return  Result.ok(message);
    }
}
