package com.zhx.security.browser.logout;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zhx.result.Result;
import org.apache.commons.lang.StringUtils;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author zhanghaixuan
 * @create 2017/11/27-下午9:13
 **/
public class ZhxLogoutSuccessHandler implements LogoutSuccessHandler {

    private ObjectMapper objectMapper = new ObjectMapper();

    private String logoutUrl;

    public ZhxLogoutSuccessHandler(String logoutUrl){
        this.logoutUrl =logoutUrl;
    }

    @Override
    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        //如何配了页面 跳转页面 如果没有 返回json
        if(StringUtils.isBlank(logoutUrl)){
            response.setContentType("application/json,charset=UTF-8");

            response.getWriter().write(objectMapper.writeValueAsString(Result.ok("退出登录")));
        }else {
            response.sendRedirect(logoutUrl);
        }
    }
}
