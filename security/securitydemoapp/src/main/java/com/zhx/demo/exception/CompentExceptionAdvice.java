package com.zhx.demo.exception;

import com.zhx.StrException;
import com.zhx.result.Result;
import org.springframework.boot.context.config.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * 统一错误处理
 *
 * @author zhx
 * @create 2017-10-24 11:41
 **/

@ControllerAdvice
public class CompentExceptionAdvice {

    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public Result defaultErrorHandler(Exception e){
        if(e instanceof StrException){
            return Result.error(e.getMessage());
        }

        if(e instanceof HttpMessageNotReadableException){
            return Result.exception("调用错误!");
        }


        if(e instanceof ResourceNotFoundException){
            return Result.resource(e.getMessage());
        }

        return Result.exception(e.getMessage());
    }
}
