package com.zhx.demo.dto;

/**
 * 用户查询封装
 *
 * @author zhx
 * @create 2017-11-01 15:04
 **/
public class UserQueryCondition {

    private String userName;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
