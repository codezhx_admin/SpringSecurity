package com.zhx.demo.bindingresult;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

import java.util.List;

/**
 * 通用错误检查
 *
 * @author zhx
 * @create 2017-11-01 14:25
 **/
public class BindingResultCheckUtil {

    public static String hasErrors(BindingResult errors){
        //如果有错误
        if(errors.hasErrors()){

            List<ObjectError> allErrors = errors.getAllErrors();

            FieldError defaultMessage = (FieldError) allErrors.get(0);

            String defaultMessageStr = defaultMessage.getDefaultMessage();

            return  defaultMessageStr;
        }else{
            return "";
        }
    }
}
