package com.zhx.demo.security;

import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionSignUp;

/**
 * 注:这个方法需要在客户端自己实现 实现的定义为 当用户完成第三方授权后并且返回了用户信息 是否需要注册
 * 如果不需要注册 spring将自己实现用户的新增 就是将第三方的信心直接作为用户
 * @author zhanghaixuan
 * @create 2017/11/26-上午10:53
 **/
//@Component
public class DemoConnectSignUp implements ConnectionSignUp {
    @Override
    public String execute(Connection<?> connection) {
        return connection.getDisplayName();
    }
}
