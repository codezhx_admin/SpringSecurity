package com.zhx.app.social.openid;

import com.zhx.core.common.constants.SecurityConstants;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.util.Assert;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * openid 登录的拦截器
 * @author zhanghaixuan
 * @create 2017/12/14-上午8:53
 **/
public class OpenIdAuthenticationFilter extends AbstractAuthenticationProcessingFilter {

    private String openIdParamter = SecurityConstants.DEFAULT_PARAMETER_NAME_OPENID;
    private String providerIdParamter = SecurityConstants.DEFAULT_PARAMETER_NAME_PROVIDERID;
    private boolean postOnly = true;

    public OpenIdAuthenticationFilter() {
        super(new AntPathRequestMatcher(SecurityConstants.DEFAULT_LOGIN_PROCESSING_URL_OPENID,"POST"));
    }


    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException, IOException, ServletException {
        if (postOnly && !request.getMethod().equals("POST")) {
            throw new AuthenticationServiceException(
                    "Authentication method not supported: " + request.getMethod());
        }

        String openId = obtainOpenId(request);
        String providerId = obtainproviderId(request);

        if (openId == null) {
            openId = "";
        }

        if (providerId == null) {
            providerId = "";
        }

        openId = openId.trim();
        providerId = providerId.trim();

        OpenIdAuthenticationToken openIdAuthenticationToken=new OpenIdAuthenticationToken(openId, providerId);

        // Allow subclasses to set the "details" property
        setDetails(request, openIdAuthenticationToken);

        return this.getAuthenticationManager().authenticate(openIdAuthenticationToken);
    }

    protected String obtainOpenId(HttpServletRequest request) {
        return request.getParameter(openIdParamter);
    }

    protected String obtainproviderId(HttpServletRequest request) {
        return request.getParameter(providerIdParamter);
    }


    protected void setDetails(HttpServletRequest request,
                              OpenIdAuthenticationToken authRequest) {
        authRequest.setDetails(authenticationDetailsSource.buildDetails(request));
    }


    public String getOpenIdParamter() {
        return openIdParamter;
    }

    public void setOpenIdParamter(String openIdParamter) {
        Assert.hasText(openIdParamter, "Username parameter must not be empty or null");
        this.openIdParamter = openIdParamter;
    }

    public String getProviderIdParamter() {
        return providerIdParamter;
    }

    public void setProviderIdParamter(String providerIdParamter) {
        this.providerIdParamter = providerIdParamter;
    }

    public boolean isPostOnly() {
        return postOnly;
    }

    public void setPostOnly(boolean postOnly) {
        this.postOnly = postOnly;
    }
}
