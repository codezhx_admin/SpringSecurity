package com.zhx.app.social;

import com.zhx.core.social.ZhxSpringSocialConfigurer;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;

/**
 * BeanPostProcessor  所有的bean初始化之前或者之后都会经过这个方法
 * @author zhx
 * @create 2017-12-14 14:35
 **/
@Component
public class SpringSocialConfigurerPostProcessor implements BeanPostProcessor{

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        if(StringUtils.equals(beanName,"zhxSpringSocialConfigurer")){
            ZhxSpringSocialConfigurer zhxSpringSocialConfigurer =(ZhxSpringSocialConfigurer)  bean;

            zhxSpringSocialConfigurer.signupUrl("/social/signup");
            return zhxSpringSocialConfigurer;
        }
        return bean;
    }
}
