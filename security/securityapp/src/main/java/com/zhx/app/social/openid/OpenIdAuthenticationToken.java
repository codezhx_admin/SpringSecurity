package com.zhx.app.social.openid;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.SpringSecurityCoreVersion;

import java.util.Collection;

/**
 * 注:使用社交登录的时候通过openid 等信息登录 其实就是做了另外一种登录方式
 * 构建登录信息
 * @author zhanghaixuan
 * @create 2017/12/14-上午8:41
 **/
public class OpenIdAuthenticationToken extends AbstractAuthenticationToken{

    private static final long serialVersionUID = SpringSecurityCoreVersion.SERIAL_VERSION_UID;

    //weixin qq 等
    private final Object principal;
    //openid
    private String providerId;

    public OpenIdAuthenticationToken(String openId,String providerId) {
        super(null);
        this.principal = openId;
        this.providerId = providerId;
        super.setAuthenticated(false);
    }


    public OpenIdAuthenticationToken(Collection<? extends GrantedAuthority> authorities,Object principal) {
        super(authorities);
        this.principal = principal;
        super.setAuthenticated(true);
    }




    @Override
    public void setAuthenticated(boolean authenticated) throws IllegalArgumentException {
        if(authenticated){
            throw new IllegalArgumentException("wrong authenticated");
        }

        super.setAuthenticated(false);
    }


    @Override
    public void eraseCredentials() {
        super.eraseCredentials();
    }

    @Override
    public Object getCredentials() {
        return null;
    }

    @Override
    public Object getPrincipal() {
        return principal;
    }

    public String getProviderId() {
        return providerId;
    }

    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }
}
