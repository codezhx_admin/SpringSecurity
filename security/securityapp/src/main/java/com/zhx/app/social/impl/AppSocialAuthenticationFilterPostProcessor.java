package com.zhx.app.social.impl;

import com.zhx.core.social.SocialAuthenticationFilterPostProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.social.security.SocialAuthenticationFilter;

/**
 * 注:实现了 当用户成功登陆后的处理方式
 * @author zhx
 * @create 2017-12-14 11:16
 **/
public class AppSocialAuthenticationFilterPostProcessor  implements SocialAuthenticationFilterPostProcessor {
    @Autowired
    private AuthenticationSuccessHandler imoocAuthenticationSuccessHandler;

    /* (non-Javadoc)
     * @see com.imooc.security.core.social.SocialAuthenticationFilterPostProcessor#process(org.springframework.social.security.SocialAuthenticationFilter)
     */
    @Override
    public void process(SocialAuthenticationFilter socialAuthenticationFilter) {
        socialAuthenticationFilter.setAuthenticationSuccessHandler(imoocAuthenticationSuccessHandler);
    }
}
