package com.zhx.app;

import com.zhx.app.social.openid.OpenIdAuthenticationConfig;
import com.zhx.core.authentication.mobile.SmsCodeAuthenticationSecurityConfig;
import com.zhx.core.authorize.AuthorizeConfigManager;
import com.zhx.core.common.constants.SecurityConstants;
import com.zhx.core.common.properties.SecurityProperties;
import com.zhx.core.validate.image.ImageCodeSecurityConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.session.InvalidSessionStrategy;
import org.springframework.social.security.SpringSocialConfigurer;

/**
 * @author zhanghaixuan
 * @create 2017/12/10-下午5:39
 *
 * EnableResourceServer  实现资源服务器
 **/
@Configuration
@EnableResourceServer
public class zhxResourceServerConfig extends ResourceServerConfigurerAdapter{

    //注入自定义的成功处理器
    @Autowired
    private AuthenticationSuccessHandler myAuthentionSuccessHandler;

    //注入自定义的失败处理器
    @Autowired
    private AuthenticationFailureHandler myAuthentionFailedHandler;

    //自定义的配置项目
    @Autowired
    private SecurityProperties securityProperties;

    @Autowired
    private UserDetailsService myUserDetailService;

    //注入自己实现的短信登录校验过程的配置
    @Autowired
    private SmsCodeAuthenticationSecurityConfig smsCodeAuthenticationSecurityConfig;

    @Autowired
    private ImageCodeSecurityConfig imageCodeSecurityConfig;

    @Autowired
    private SpringSocialConfigurer zhxSocialSpringConfigurer;

    @Autowired
    private OpenIdAuthenticationConfig openIdAuthenticationConfig;


    @Autowired
    private AuthorizeConfigManager authorizeConfigManager;

    /**
     * 配置项目  相当于brower中的配置
     * @param http
     * @throws Exception
     */
    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.formLogin()
                //自定义登陆页面
                .loginPage(SecurityConstants.DEFAULT_UNAUTHENRICATION_URL)
                .loginProcessingUrl(SecurityConstants.DEFAULT_LOGIN_PROCESSING_URL_FORM)
                //指定自定义的成功处理器
                .successHandler(myAuthentionSuccessHandler)
                .failureHandler(myAuthentionFailedHandler);

        http.apply(smsCodeAuthenticationSecurityConfig)
                .and()
                .apply(imageCodeSecurityConfig)
                 .and()
                .apply(zhxSocialSpringConfigurer)
                .and()
                .apply(openIdAuthenticationConfig)
                .and()
                .csrf()
                .disable();

        authorizeConfigManager.config(http.authorizeRequests());
    }
}
